from django.shortcuts import render
from django.views.generic import ListView, DetailView, CreateView, DeleteView, UpdateView
from django.contrib.auth.mixins import LoginRequiredMixin
from receipts.models import Receipt, ExpenseCategory, Account
from django.urls import reverse_lazy
from receipts.forms import ReceiptForm
from django.conf import settings
USER_MODEL = settings.AUTH_USER_MODEL

# Create your views here.

#receipt views
class ReceiptListView(LoginRequiredMixin, ListView):
    model = Receipt
    template_name = "receipts/list.html"
    def get_queryset(self):
        return Receipt.objects.filter(owner=self.request.user)


class ReceiptCreateView(LoginRequiredMixin, CreateView):
    model = Receipt
    form_class = ReceiptForm
    template_name = "receipts/new.html"
    success_url = reverse_lazy("receipts_list_url")

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['owner'] = self.request.user
        return kwargs

    def form_valid(self, form):
        form.instance.owner = self.request.user
        return super().form_valid(form)


class ReceiptUpdateView(LoginRequiredMixin, UpdateView):
    model = Receipt
    form_class = ReceiptForm
    template_name = "receipts/edit.html"
    success_url = reverse_lazy("receipts_list_url")
    # fields = [
    #     "vendor",
    #     "total",
    #     "tax",
    #     "date",
    #     "category",
    #     "account",
    # ]

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['owner'] = self.request.user
        return kwargs


    def form_valid(self, form):
        form.instance.owner = self.request.user
        return super().form_valid(form)


class ReceiptDeleteView(LoginRequiredMixin, DeleteView):
    model = Receipt
    template_name = "receipts/delete.html"
    success_url = reverse_lazy("receipts_list_url")



#category views
class CategoryListView(LoginRequiredMixin, ListView):
    model = ExpenseCategory
    template_name = "categories/list.html"
    context_object_name = "categories"

    def get_queryset(self):
        return ExpenseCategory.objects.filter(owner=self.request.user).order_by("name")


class CategoryCreateView(LoginRequiredMixin, CreateView):
    model = ExpenseCategory
    fields = [
        "name",
    ]
    template_name = "categories/new.html"
    success_url = reverse_lazy("category_list_url")

    def form_valid(self, form):
        form.instance.owner = self.request.user
        return super().form_valid(form)


class CategoryUpdateView(LoginRequiredMixin, UpdateView):
    model = ExpenseCategory
    fields = [
        "name",
    ]
    template_name = "categories/edit.html"
    success_url = reverse_lazy("category_list_url")
    def form_valid(self, form):
        form.instance.owner = self.request.user
        return super().form_valid(form)


class CategoryDeleteView(LoginRequiredMixin, DeleteView):
    model = ExpenseCategory
    template_name = "categories/delete.html"
    success_url = reverse_lazy("category_list_url")



#account views
class AccountListView(LoginRequiredMixin, ListView):
    model = Account
    template_name = "accounts/list.html"

    def get_queryset(self):
        return Account.objects.filter(owner=self.request.user).order_by("name")


class AccountCreateView(LoginRequiredMixin, CreateView):
    model = Account
    fields = [
        "name",
        "number",
    ]
    template_name = "accounts/new.html"
    success_url = reverse_lazy("account_list_url")
    def form_valid(self, form):
        form.instance.owner = self.request.user
        return super().form_valid(form)


class AccountUpdateView(LoginRequiredMixin, UpdateView):
    model = Account
    fields = [
        "name",
        "number",
    ]
    template_name = "accounts/edit.html"
    success_url = reverse_lazy("account_list_url")
    def form_valid(self, form):
        form.instance.owner = self.request.user
        return super().form_valid(form)
    pass


class AccountDeleteView(LoginRequiredMixin, DeleteView):
    model = Account
    template_name = "accounts/delete.html"
    success_url = reverse_lazy("account_list_url")
